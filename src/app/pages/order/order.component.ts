import { Component, OnInit } from '@angular/core';
import { IOrder, IUserLoginOrders } from 'src/app/models/order';
import { OrderService } from "../../services/order/order.service";
import { UserService } from "../../services/user/user.service";
import { IUser } from "../../models/users";
import { ActivatedRoute, Router } from "@angular/router";
import { IColumn } from 'src/app/models/column';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  cols: IColumn[];

  orders: IOrder[] | any;

  user: IUser | any;

  showModal: boolean = false;

  orderId: string | any;

  orderName: string | any;

  orderDate: string | any;

  ordersApproved: number = 0;

  first = 0;

  rows = 10;

  constructor(private orderService: OrderService,
              private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.initOrders();
  }

  initOrders(): void {
    const userId = <string>this.userService.getUser()?.id;

    this.userService.getUserById(userId).subscribe((user) => {
      this.user = user;

      if (this.user.role === 'admin') {
        this.setColsAdmin();
        this.orderService.getOrderAll().subscribe((orders) => {
          this.getAdminOrders(orders);
        });
      }

      if (this.user.role === 'user') {
        this.setColsUser();
        this.getUserOrders(userId);
      }
    });
  }

  getUserOrders(id: string) {
    this.orderService.getOrders(id).subscribe((orders) => {
      this.orders = orders;

      for (let i = 0; i < this.orders.length; i++) {
        this.ordersApproved = this.orders.filter((el: any) => el.orderType === 'approvedOrder').length;
      }
    });
  }


  getAdminOrders(allOrders: any) {
    const orderArr = <any>[];

    this.userService.getUserAll().subscribe((users: any) => {      
      let userMap = new Map();

      users.forEach((user: any) => {
        userMap.set(user._id, user.login);
      });

      for (let i = 0; i < allOrders.length; i++) {
        let login = userMap.get(allOrders[i].userId);
        orderArr.push({ ...allOrders[i], login });
      }

      this.orders = orderArr;
    })
  }

  showDeleteOrderDialog(order: IOrder) {
    this.orderId = order._id;
    this.orderName = order.nameTicket;
    this.orderDate = order.date;
    this.showModal = true;
  }

  deleteOrder() {
    this.showModal = false;
    this.orderService.deleteOrderById(this.orderId).subscribe(() => {
      this.initOrders();
    });
  }

  goToTicketInfoPage(ev: any) {
    this.router.navigate(['/tickets/ticket'], { queryParams: { id: ev.tourId }, relativeTo: this.route }
    );
  }

  changeOrderType(ev: any, orderType: 'canceledOrder' | 'approvedOrder' | 'newOrder') {
    for (let i = 0; i < this.orders.length; i++) {
      if (this.orders[i]._id === ev._id) {
        this.orderService.setOrder(this.orders[i]);
        const order = <IOrder>this.orderService.getOrder();
        order.orderType = orderType;     
        this.orderService.updateOrderById(ev._id, order);
        break;
      }
    }
    this.initOrders();
  }

  setColsAdmin(){
    this.cols = [
      { field: 'index', header: 'N' },
      { field: 'btn', header: 'Изменение статуса'},
      { field: 'orderType', header: 'Статус заказа' },
      { field: 'login', header: 'Логин' },
      { field: 'ticket', header: 'Данные о туре' },
      { field: 'price', header: 'Цена, РУБ' },
      { field: 'name', header: 'Сведения о заказчике' },
      { field: 'phone', header: 'Контакты заказчика' },
    ];
  }

  setColsUser(){
    this.cols = [
      { field: 'index', header: 'N' },
      { field: 'btn', header: 'Изменение статуса'},
      { field: 'orderType', header: 'Статус' },
      { field: 'nameTicket', header: 'Тур' },
      { field: 'price', header: 'Цена, РУБ' },
      { field: 'date', header: 'Дата начала тура' },
      { field: 'location', header: 'Город' },
      { field: 'firstName', header: 'Имя' },
      { field: 'lastName', header: 'Фамилия' },
      { field: 'phone', header: 'Телефон' },
      { field: 'eMail', header: 'e-mail' },
    ];
  }
  
  next() {
    this.first = this.first + this.rows;
  }

  prev() {
    this.first = this.first - this.rows;
  }

  reset() {
    this.first = 0;
  }

  isLastPage(): boolean {
    return this.orders ? this.first === (this.orders.length - this.rows) : true;
  }

  isFirstPage(): boolean {
    return this.orders ? this.first === 0 : true;
  }
}
