import { Component, OnInit } from '@angular/core';
import { TicketsRestService } from "../../../services/rest/tickets-rest.service";
import { IStatisticTourLocation, ITour, ITourOrder } from "../../../models/tours";
import { OrderService } from "../../../services/order/order.service";
import { IOrder } from "../../../models/order";
import { City } from 'src/app/models/city';
import { IColumn } from 'src/app/models/column';
import { UserService } from 'src/app/services/user/user.service';
import { IUser } from 'src/app/models/users';


@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.scss']
})
export class StatisticComponent implements OnInit {

  cols!: IColumn[];

  cities: City[];

  selectedCity: City | null;

  dataStatisticsByCity: any;

  optionsStatisticsByCity: any;

  dataStatisticsBySeason: any;

  optionsStatisticsBySeason: any;

  dataPopularDestination: any;

  optionsPopularDestination: any;
  
  orderPopularDestination: number[];

  dataOrderStatus: any;

  optionsOrderStatus: any;

  tickets: ITour[];

  orders: IOrder[] | any;

  orderCount: number[] = [];

  ticketId: string;

  ticketOrder: ITourOrder[] | any;

  locationStats: { [key: string]: number } | any;

  user: IUser | any;

  constructor(private ticketsRestService: TicketsRestService,
              private orderService: OrderService,
              private userService: UserService,
  ) {
  }

  ngOnInit(): void {
    this.initStatistic();

    this.cols = [
      { field: 'name', header: 'Тур' },
      { field: 'price', header: 'Цена' },
      { field: 'location', header: 'Город' },
      { field: 'date', header: 'Дата' },
      { field: 'countBooking', header: 'Количество брони' },
      { field: 'total', header: 'Общая сумма брони' }
    ];

    this.getData();
  }

  initStatistic(): void {
    const userId = <string>this.userService.getUser()?.id;
    this.userService.getUserById(userId).subscribe((user) => {
      this.user = user;    
    });
  }

  getData(): void {
    this.orderService.getOrderAll().subscribe((orders) => {
      this.orders = orders;
      this.statisticOrderStatus(this.orders);

      this.ticketsRestService.getTickets().subscribe((tickets) => {
        this.tickets = tickets;

        this.createListCities(tickets);
        this.combineTicketsAndOrders();

        const locationStats = this.getStatisticLocation();
        this.setBasicData(locationStats)
      });
    });
  }

  statisticOrderStatus(orders: IOrder[]){
    let newOrder: number = 0;
    let approvedOrder: number = 0;
    let canceledOrder: number = 0;
    orders.forEach(order =>{
      if(order.orderType === 'newOrder') newOrder++;
      if(order.orderType === 'approvedOrder') approvedOrder++;
      if(order.orderType === 'canceledOrder') canceledOrder++;  
    })
    
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');

    this.dataOrderStatus = {
        labels: ['В работе', 'Подтверждён', 'Отменён'],
        datasets: [
            {
                data: [newOrder, approvedOrder, canceledOrder],
                backgroundColor: [documentStyle.getPropertyValue('--blue-500'), documentStyle.getPropertyValue('--green-500'), documentStyle.getPropertyValue('--pink-500')],
                hoverBackgroundColor: [documentStyle.getPropertyValue('--blue-400'), documentStyle.getPropertyValue('--green-400'), documentStyle.getPropertyValue('--pink-400')]
            }
        ]
    };


    this.optionsOrderStatus = {
        cutout: '60%',
        plugins: {
            legend: {
                labels: {
                    color: textColor
                }
            }
        }
    };
}
  

  onSelectedCity() {   
    this.orderCount = [];
    
    const ordersByCity: City[] = this.orders.filter((order: IOrder) => order.location == this.selectedCity?.name);
    const date: number[] = [];

    ordersByCity.forEach((order: any) => {
      date.push(new Date(order.date).getMonth() + 1);
    });

    for (let i = 1; i < 13; i++) {
      if ((date.filter(v => v === i)).length !== 0) this.orderCount.push((date.filter(v => v === i)).length);
      else this.orderCount.push(0);
    }

    this.optionsStatisticsBySeason = {
      maintainAspectRatio: false,
      aspectRatio: 2.0,
      scales: {
        y: {
          beginAtZero: true,
        }
      }
    }

    const documentStyle = getComputedStyle(document.documentElement);
    
    this.dataStatisticsBySeason = {
      labels: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
      datasets: [
        {
          label: 'Количество туров',
          data: this.orderCount,
          fill: false,
          borderDash: [5, 5],
          tension: 0.4,
          borderColor: documentStyle.getPropertyValue('--teal-500')
        }
      ]
    };
    
  }

  createListCities(tickets: any[]) {
    const allCity: string[] = [];
    tickets.forEach((ticket) => {
      allCity.push(ticket.location);
    });

    const selectedСities = allCity.filter((item, index) => {
      return (allCity.indexOf(item) === index)
    });

    const sortCities = selectedСities.sort((a, b) => a.localeCompare(b));
    this.popularDestination(sortCities, this.orders);

    const cities: City[] = [];
    sortCities.forEach((city) => {
      cities.push({
        name: city,
        code: city
      })
    });

    this.cities = cities;
    this.selectedCity = cities[cities.length - 1];
    this.onSelectedCity();
  }

  //Статистика по направлениям 
  popularDestination(cities: any, orderCount: number[]){
    const countOrdersDestination: number[] = [];

    cities.forEach((city: string) => {
      let count: number = 0;
      orderCount.forEach((order: IOrder | any) => {
        if(city === order.location) count++;
      })
      return countOrdersDestination.push(count);
    })
    
    const stylePopularDestination = getComputedStyle(document.documentElement);
    const textColor = stylePopularDestination.getPropertyValue('--text-color');
    const textColorSecondary = stylePopularDestination.getPropertyValue('--text-color-secondary');
    const surfaceBorder = stylePopularDestination.getPropertyValue('--surface-border');

    this.dataPopularDestination = {
      labels: cities,
      datasets: [
        {
          label: 'Количество заказов',
          data: countOrdersDestination,
          backgroundColor: ['rgba(255, 159, 64, 0.2)', 'rgba(75, 192, 192, 0.2)', 'rgba(54, 162, 235, 0.2)', 'rgba(153, 102, 255, 0.2)'],
          borderColor: ['rgb(255, 159, 64)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)'],
          borderWidth: 1
        }
      ]
    };

    this.optionsPopularDestination = {
      maintainAspectRatio: false,
      aspectRatio: 1.5,
      plugins: {
        legend: {
          labels: {
            color: textColor
          }
        }
      },
      scales: {
        y: {
          beginAtZero: true,
          ticks: {
            beginAtZero: true,
            stepSize: 1,
            color: textColorSecondary
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        },
        x: {
          ticks: {
            color: textColorSecondary
          },
          grid: {
            color: surfaceBorder,
            drawBorder: false
          }
        }
      }
    };
  }

  combineTicketsAndOrders(): void {
    this.ticketOrder = [];
    for (let i = 0; i < this.tickets.length; i++) {
      const ticketId = this.tickets[i]._id;
      let countBooking = this.orders.filter((el: any) => el.tourId === ticketId && el.orderType !== 'canceledOrder').length;
      this.ticketOrder.push({ ...this.tickets[i], countBooking });
    }
    return this.ticketOrder;
  }
// 1. Объявляется функция getStatisticLocation(), которая возвращает значение IStatisticTourLocation[] .
  // 2. Объявляется пустой объект locationStats.
  // 3. Цикл forEach используется для перебора всех билетов.
  // 4. TicketLocation присваивается значение местоположения текущего билета.
  // 5. Условие if используется для проверки того, существует ли уже ticketLocation в объекте locationStats.
  // 6. Если ticketLocation не существует, то ticketLocation добавляется к объекту locationStats со значением 0.
  // 7. Если ticketLocation существует, то значение ticketLocation увеличивается на 1.
  // 8. Возвращается объект locationStats

  getStatisticLocation(): IStatisticTourLocation[] | any {
    let locationStats: any = {};
    this.tickets.forEach((ticket) => {
      const ticketLocation = ticket.location;
      if (!locationStats[ticketLocation]) {
        locationStats[ticketLocation] = 0;
      }
      locationStats[ticketLocation] += 1;
    });
    return locationStats;
  }
// 1. Функция setBasicData() объявляется с параметром locationStats, который представляет собой массив типа
  // IStatisticTourLocation или любого другого.
  // 2. Объявляется переменная labels, которой присваивается массив ключей параметра locationStats.
  // 3. Свойству basicData класса присваивается объект со свойством labels, которому присваивается переменная labels,
  // и свойству наборов данных, которое присваивается массиву с одним объектом.
  // 4. Объект в массиве наборов данных имеет свойство label, для которого задана строка, свойство backgroundColor,
  // для которого задан шестнадцатеричный код, и свойство data, для которого задана карта переменной labels,
  // для которой задан параметр locationStats.

  setBasicData(locationStats: IStatisticTourLocation[] | any): void {
    const labels: string[] = Object.keys(locationStats);
    this.dataStatisticsByCity = {
      labels: labels,
      datasets: [
        {
          label: 'Количество туров',
          backgroundColor: '#42A5F5',
          data: labels.map(l => locationStats[l])
        },
      ]
    };
  }
}

