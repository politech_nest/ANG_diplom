import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatisticComponent } from './statistic.component';
import { AccordionModule } from 'primeng/accordion';
import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';
import { StatisticRoutingModule } from './statistic-routing.module';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    StatisticComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    StatisticRoutingModule,
    AccordionModule,
    TableModule,
    ChartModule,
    DropdownModule 
  ]
})
export class StatisticModule { }