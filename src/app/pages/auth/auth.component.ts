import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {

  tabActive: number = 1

  constructor() {}

  isTabCaching: boolean = false;

  ngOnInit(): void {}

  tab(num:number){
    this.tabActive = num;
  }
}

