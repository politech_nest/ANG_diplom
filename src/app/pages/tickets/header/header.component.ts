import { Component, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { MenuItem } from "primeng/api";
import { UserService } from "../../../services/user/user.service";
import { IUser } from "../../../models/users";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  items: MenuItem[];
  user: IUser | null;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.items = [
      {
        label: 'Туры',
        routerLink: ['tickets-list']
      },
      {
        label: 'Заказы',
        routerLink: ['order']
      },
      {
        label: 'Статистика',
        routerLink: ['statistic']
      },
      {
        label: 'Личный кабинет ',
        routerLink: ['settings']
      },
      {
        label: 'Выход',
        routerLink: ['/auth'],
        command: (click) => {
          this.userService.removeUser();
          window.localStorage.clear();
        }
      },
    ];
    this.user = this.userService.getUser();
  }

  ngOnChanges(ev: SimpleChanges): void {
    this.items = this.initMenuItems();
  }

  initMenuItems(): MenuItem[] {
    return [
      {
        label: 'Туры',
        routerLink: ['tickets-list']
      },
      {
        label: 'Заказы',
        routerLink: ['order']
      },
      {
        label: 'Статистика',
        routerLink: ['statistic']
      },
      {
        label: 'Личный кабинет ',
        routerLink: ['settings'],
      },
      {
        label: 'Выход',
        routerLink: ['/auth'],
        command: (click) => {
          this.userService.removeUser()
        }
      },
    ];
  }
}

