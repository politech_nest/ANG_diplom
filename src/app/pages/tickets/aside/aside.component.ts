import { Component, OnDestroy, OnInit } from '@angular/core';
import { ITour, ITourTypeSelect } from "../../../models/tours";
import { TicketsService } from "../../../services/tickets/tickets.service";
import { SettingsService } from "../../../services/settings/settings.service";
import { UserService } from "../../../services/user/user.service";
import { IUser } from "../../../models/users";
import { BlocksStyleDirective } from "../../../directive/blocks-style.directive";
import { AsideService } from 'src/app/services/aside/aside.service';
import { Subscription } from "rxjs";
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { defaultFilter } from 'src/app/const/defaultFilter';


@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss'],
  providers: [MessageService]
})


export class AsideComponent implements OnInit, OnDestroy {

  user: IUser | null;
  time: Date;
  _hasView: boolean = false;
  tickets: ITour[];
  blockDirective: BlocksStyleDirective;
  viewAside: boolean;
  dateValue: string;
  typeTour: { label: string, value: string };
  displayDeleteTours: boolean = false;

  tourTypes: ITourTypeSelect[] = [
    { label: 'Все', value: 'all' },
    { label: 'Одиночный', value: 'single' },
    { label: 'Групповой', value: 'multi' }
  ]

  private timeInterval: number;
  private viewAsideSub: Subscription;

  constructor(private ticketService: TicketsService,
    private settingsService: SettingsService,
    private userService: UserService,
    private asideService: AsideService,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit(): void {
    this.resetFilter();   

    // блок, определяющий правило отображения настроек для администратора
    this.user = this.userService.getUser();
    if (this.user?.role === 'admin') {
      this._hasView = true;
    }

    this.viewAsideSub = this.asideService.viewAside$.subscribe((data) =>
      this.viewAside = data
    );

    this.ticketService.ticketUpdateSubject$.subscribe((data) => {
      this.tickets = data;
    });

    this.timeInterval = window.setInterval(() => {
      this.time = new Date();
    }, 1000)
  }

  changeTourFilter(filterType: string, event?: any): void {    
    if (filterType === 'type') this.ticketService.filter.type = event.value as ITourTypeSelect;
    if (filterType === 'date') this.ticketService.filter.date = event as string;
    if (filterType === 'priceMin') {
      this.ticketService.filter.priceMin = true;
      this.ticketService.filter.priceMax = false;
    }
    if (filterType === 'priceMax') {
      this.ticketService.filter.priceMax = true;
      this.ticketService.filter.priceMin = false;
    }
    if (filterType === 'find') {
      this.ticketService.filter.find = event.target.value;
    }

    if (filterType === 'reset') {
      this.resetFilter();
    }
    this.ticketService.updateTour(this.ticketService.filter)
  }

  resetFilter(): void { 
  this.ticketService.filter = {
      type: { label: 'Все', value: 'all' },
      date: '',
      priceMin: false,
      priceMax: false,
      find: ''
    };

    this.typeTour = { label: 'Все', value: 'all' };
    this.dateValue = '';

    // this.ticketService.filter = defaultFilter;
    this.ticketService.updateTour(this.ticketService.filter)
  }

  initSettingsData(): void {
    this.settingsService.loadUserSettingsSubject(
      { saveToken: false }
    )
  };

  deleteTours(): void {
    this.ticketService.deleteAllTours().subscribe(() => {
      this.displayDeleteTours = false;
      this.ticketService.filter.deleteAll = true;
      this.ticketService.updateTour(this.ticketService.filter);
    })
  }

  ngOnDestroy(): void {
    if (this.timeInterval) {
      window.clearInterval(this.timeInterval);
    }
    this.viewAsideSub.unsubscribe();
  }
}
