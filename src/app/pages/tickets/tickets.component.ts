import { Component, OnInit } from '@angular/core';
import { ITour } from "../../models/tours";
import { TicketsRestService } from "../../services/rest/tickets-rest.service";
import { NavigationEnd, Router, RoutesRecognized } from '@angular/router';


@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss']
})
export class TicketsComponent implements OnInit {
  tickets: ITour[];
  url: string = '/tickets/tickets-list';

  constructor(private ticketsRestService: TicketsRestService,
              private router: Router) {

    this.router.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        this.url = event.url;              
      }     
    })
  }

  ngOnInit(): void {
      this.ticketsRestService.getTickets().subscribe((data) => {
      this.tickets = data;
    });
  }
}
