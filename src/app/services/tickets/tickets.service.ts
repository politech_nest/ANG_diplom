import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs";
import { IFilter } from 'src/app/models/filter';
import { ICustomTourLocation, INearestTour, ITour, ITourLocation } from "../../models/tours";
import { TicketsRestService } from "../rest/tickets-rest.service";
import { Countries } from 'src/app/models/countries';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  filter: IFilter = <IFilter>{};
  // слушает события фильтра
  private ticketSubject = new Subject<IFilter>();
  // слушает события изменений списка туров
  private ticketUpdateSubject = new Subject<ITour[]>();
  // доступен только для прочтения данных
  readonly ticketUpdateSubject$ = this.ticketUpdateSubject.asObservable();

  constructor(private ticketsServiceRest: TicketsRestService) {
  }

  getTickets(): Observable<ITour[]> {
    return this.ticketsServiceRest.getTickets();
  }

  readonly ticketType$ = this.ticketSubject.asObservable();

  updateTour(filter: IFilter): void {
    this.ticketSubject.next(filter);
  }

  getError() {
    return this.ticketsServiceRest.getRestError()
  }

  sendTourData(data: any): Observable<any> {
    return this.ticketsServiceRest.sendTourData(data);
  }

  updateTicketList(data: ITour[]) {    
    this.ticketUpdateSubject.next(data);
  }

  getTicketById(id: string): Observable<ITour> {
    return this.ticketsServiceRest.getTicketById(id);
  }

  createTour(body: any) {
    return this.ticketsServiceRest.createTour(body);
  }

  deleteTourById(id: string): Observable<ITour> {
    return this.ticketsServiceRest.deleteTourById(id);
  }

  deleteAllTours(): any {
    return this.ticketsServiceRest.deleteAllTours();
  }

  getCountries(): Observable<Countries[]> {
    return this.ticketsServiceRest.getCountries();
  }
}
