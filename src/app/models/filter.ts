import { ITourTypeSelect } from "./tours";

export interface IFilter {
  type?: ITourTypeSelect,
  date?: any,
  priceMin?: any,
  priceMax?: any,
  find?: string,
  reset?: boolean,
  deleteAll?: boolean
}



