export interface Countries {
    flag_url : string;
    name : string;
    iso_code2 : string;
    iso_code3 : string;
  }