import { Component } from '@angular/core';
import { ObservableExampleService } from "./services/testing/observable-example.service";
import { ConfigService } from "./services/config/config.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Тянь-Шань тур';
  prop: string;

  constructor(private testing: ObservableExampleService,
    private config: ConfigService) {

  }

  ngOnInit() {
    this.config.configLoad()
  }
}
